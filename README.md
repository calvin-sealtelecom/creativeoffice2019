# Creative Office - Projects 2019

## Horus Digital Signage - TRT10
1.8.03 version:\
![Imgur](https://i.imgur.com/T67hlm6.gif)

![Imgur](https://i.imgur.com/nKDgpdd.gif)

## Bradesco Interactive
stable version:\
![Imgur](https://i.imgur.com/KI06cr6.gif)

alpha version:\
![Imgur](https://i.imgur.com/Be481wI.gif)

## BRF - Relatório personalizado
stable version:\
![Imgur](https://i.imgur.com/IKCsWow.png)

alpha version:\
![Imgur](https://i.imgur.com/osM7d9H.gif)

## Portal Agendador Caixa Econômica Federal
stable version:\
![Imgur](https://i.imgur.com/pdAPbXy.gif)

alpha version:\
![Imgur](https://i.imgur.com/OqXQfS7.gif)

## Add-on Equinox
latest version:\
![Imgur](https://i.imgur.com/OYKgeBd.png)

staging version:\
![Imgur](https://i.imgur.com/GliovIL.gif)

## Horus Realidade Aumentada
latest version:\
![Imgur](https://i.imgur.com/dpLvB14.gif)

## Magic Mirror Virtual Dressing
0.0.2-alpha version:\
![Imgur](https://i.imgur.com/ca3J8tx.gif)

## Seal Conference
latest version:\
![Imgur](https://i.imgur.com/1aQfZsD.png)

## Horus Digital Signage Player - webOSSignage
latest version:\
![Imgur](https://i.imgur.com/cVo4Vay.gif)

staging version:\
![Imgur](https://i.imgur.com/7d3FQHQ.gif)

## Horus Digital Signage - Bluesoft
1.8.04-alpha version:\
![Imgur](https://i.imgur.com/ZHWBI5z.png)

## Horus Digital Signage - Unavaible Modules
working in progress:\
![Imgur](https://i.imgur.com/TcQIdZq.gif)

working in progress:\
![Imgur](https://i.imgur.com/LyQoLNH.png)

## VideoArtisticWall
toy version:\
![Imgur](https://i.imgur.com/Al3nf66.gif)

## Action Mixer
Input by streaming in alpha development stage